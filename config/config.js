var config = {
	translation: {
		tokenServiceUrl: "https://api.cognitive.microsoft.com/sts/v1.0/issueToken",
		translateUrl: "https://api.microsofttranslator.com/v2/http.svc/TranslateArray",
		translateUrlSingle: "https://api.microsofttranslator.com/v2/http.svc/Translate",
		key1: "d636865d970f499a9f36de9f4528b40f",
		key2: "859e0676a2e94965b4631c91f2fc528b",
		ajax: {
			hostname: "api.microsofttranslator.com",
			path: "/v2/Ajax.svc/TranslateArray",
		}
	},
	db: {
		name: 'zetrovbox',
		mongoAtlasConnectionString: 'mongodb://zetrovbox:HJmvLgYXwd566x0S@cluster0-shard-00-00-qfos0.gcp.mongodb.net:27017,cluster0-shard-00-01-qfos0.gcp.mongodb.net:27017,cluster0-shard-00-02-qfos0.gcp.mongodb.net:27017/zetrovbox?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true'
	}
};

module.exports = config;