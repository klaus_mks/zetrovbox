var express = require('express');        
var app = express();    
var cors = require('cors');             
var bodyParser = require('body-parser');
var config = require('./config/config');
var pagerHelper = require('./libs/helpers/pager');
var fs = require('fs');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        
var router = express.Router();       

var mongoose   = require('mongoose');
mongoose.Promise = global.Promise;
var connection = mongoose.createConnection(config.db.mongoAtlasConnectionString);
var Post = require(__dirname + "/models/PostSchema").getModel(connection);
var maxItem = 9;

// middleware to use for all requests
router.use(function(req, res, next) {    
    console.log(req.originalUrl);    
    next();
});

router.get('/', function(req, res) {
    res.json({ message: 'apis are working properly' });   
});

router.route('/search')
	.get(function(req, res){
		var results = {};
		var numRows;
		var numPerPage = parseInt(req.query.npp, 10) || 9;
		var page = req.query.page && parseInt(req.query.page, 10) || 1;
		var numPages;		

		var searchTerm = req.query.term || null;
		var regex = function(){
			regexString = '.*' + searchTerm + '.*';		
			return new RegExp(regexString, 'i');
		}();

		if(searchTerm) {
			var excludeElements = ['in', 'at', 'on', 'the', 'under'];
			var arrTerms = searchTerm.split(' ')
				.filter(value => excludeElements.indexOf(value) == -1);
			var orCriteria = [];
			arrTerms.forEach(value => {
				orCriteria.push({name: new RegExp(value, 'i')}, {content: new RegExp(value, 'i')});
			})
		}
		
		var criteria = searchTerm ? {
			$and: [
				{$or: orCriteria, is_published: true},
			]
		}
		: {is_published: true} ;	
		Post.count(criteria, function(err, count){
			if(err)
				res.send(err);
			numRows = count;
			numPages = Math.ceil(numRows / numPerPage);			
			console.log('count ' + count);

		})
		.then(function(){
			Post.find(criteria)
				.sort({_id: -1}) 	// sort descendingly due to the order of crawled articles saved in db is reversed (new article saved first)
				.skip((page - 1) * numPerPage)
				.limit(numPerPage)
				.select('_id name url country area field level deadline image_name content is_published')
				.exec( 
					function(err, posts){
					if(err)
						res.send(err);
					results.data = posts;
					var pager = pagerHelper(numRows, page, numPerPage);
					results.pagination = pager;
					res.json(results);	
				}
			);
		});
	});

router.route('/posts')
	.get(function(req, res){
		var results = {};

		var numRows;
		var numPerPage = parseInt(req.query.npp, 10) || 9;
		var page = req.query.page && parseInt(req.query.page, 10) || 1;
		var numPages;

		Post.count({}, function(err, count){
			if(err)
				res.send(err);
			numRows = count;
			numPages = Math.ceil(numRows / numPerPage);

		})
		.then(function(){
			Post.find({})
				.sort({_id: -1}) 	// ascending sort due to the order of crawled articles saved in db is reversed (new article saved first)
				.skip((page - 1) * numPerPage)
				.limit(numPerPage)
				.select('_id name url country area field level deadline image_name content')
				.exec( 
					function(err, posts){
					if(err)
						res.send(err);
					results.data = posts;
					var pager = pagerHelper(numRows, page, numPerPage);
					results.pagination = pager;
					res.json(results);
				}
			);
		});
	});

router.route('/posts/:post_id')
	.get(function(req, res){
		Post.findById(req.params.post_id, function(err, post){
			if(err || !post){
				console.log(err);
				res.json({success: false});
			}
			else {
				if (!fs.existsSync('images/1366x350/' + post.image_name)) 
				  	post.image_name = null;			
				res.json({ success: true, data: post});	
			}
		})
		.select('_id name url country area field level deadline image_name content content_url');
	});

router.route('/posts/slug/:slug')
	.get(function(req, res){
		console.log('b slug: ' + req.params.slug);
		Post.findOne({slug: {$regex: new RegExp(req.params.slug, 'i')}}, function(err, post){
			if(err || !post){
				console.log('no error' + err);
				res.json({success: false});
			}
			else {
				if (!fs.existsSync('images/1366x350/' + post.image_name)) 
				  	post.image_name = null;			
				res.json({ success: true, data: post});	
			}
		})
		.select('_id name url country area field level deadline image_name content content_url');
	});

router.route('/posts/count/:slug')
	.get(function(req, res){		
		Post.count({slug: {$regex: new RegExp(req.params.slug, 'i')}}, function(err, number){
			if(err) {
				console.log(err);
				res.json({success: false});				
			}
			else {
				res.json({success: true, number: number});
			}
		});
	});

app.use('/api', router);
app.listen(port);
console.log('Magic happens on port ' + port);
