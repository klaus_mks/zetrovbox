var cheerio = require('cheerio');
var request = require('request');
var async = require('async');
var readability = require("readabilitySAX");
var util = require('util');
var downloader = require('../net/Download');
var slugifier = require('../helpers/my-slugifier');
var Translator = require('../net/Translator');
var config = require('../../config/config');
var dbModule = require('../dbEngine');
var cropper = require('../helpers/cropper');

var EventEmitter = require('events').EventEmitter;

var dbEngine = dbModule(config.db.name);
var slugifier = new slugifier();	
function Engine(){
	this.articles = [];
	this.results = [];
	this.nextUrl;
	this.currentPage;
	this.breakByArticleExisted = false;

	this.on('existed', function(url){
		this.breakByArticleExisted = true;
		console.log('The URL ' + url + ' has existed in DB');
	});
}

util.inherits(Engine, EventEmitter);

Engine.prototype.parseGroupsOfArticles = function(strHtml, callback){
	this.articles = [];
	var self = this;

	$ = cheerio.load(strHtml);
	var articleGroup = $('.container .grid-posts').children();
	this.nextUrl = $('.paginations .page-numbers.current').next().attr('href');
	this.currentPage = $('.paginations .page-numbers.current').next().text();

	articleGroup.each(function(index, value){
		var article = {};
		article.name = $(value).find('.content .title a').text();
		article.url = $(value).find('.content .title a').attr('href');

		if(!article.url)
			return true;
		self.articles.push(article);
	});

	var checkFinishAndProceed = function(err, res){
		if(res.length <= 0){
			callback(true);
			return false;			
		}
		else{
			self.translateAndDownloadBanner(res, self.checkFinalAndRequestNextPage.bind(self, callback));
		}	
	};

	this.parseArticleDetails(checkFinishAndProceed);
};

Engine.prototype.parseArticleDetails = function(callback){
	var calls = [];
	var self = this;
	this.articles.forEach(function(value, index, array){ 
		// add next request functions to an array to run in series
		calls.push(function(articleCallback){
			console.log('-- processing item: ' + value.name);
			console.log('-- checking the existance of :' + value.url);
			
			var checkExisted = function(callback){
				dbEngine.checkExisted(value.url, function(err, existed){
					callback(null, err, existed);
				});
			};
			
			var handleExistingStatus = function(err, existed, callback){
				if(existed){
					console.log('-- the article has already existed in DB');
					self.emit('existed', value.url);
					articleCallback(true);
				}
				else{
					console.log('-- the article has not existed in DB');
					console.log('-- requesting details of: ' + value.url);
					request(value.url, function(err, response, html){
						callback(null, err, response, html);
					});
				}
			};

			var handleArticleDetails = function(error, response, html, callback){				
				if(!error){

					$ = cheerio.load(html);
					var info = $('.scholar-info').children();
					var labels = ['Institution', 'Country', 'Area', 'Field', 'Level', 'IELTS', 'Value', 'Deadline'];
					var item = {};

					info.each(function(i, ele){
						item[labels[i].toLowerCase()] = $(ele).children('p').text();
					});
					
					item.url = value.url;
					item.ref_url = $('.single-content article a').last().attr('href');
					item.banner_url = $('.page-banner .photo').css('background-image').replace(/^url\(|\)/g, "");					

					//make a request to get reference content
					try{
						readability.get(item.ref_url, {type: "html"}, function(article){
							if(article.error){
								article.html = "";
							}
							callback(null, item, article);
						});
					}
					catch(err){
						console.log(err.message);
						callback(null, item, {html: "", error: true});
					}
					
				}
				else
					console.log(error);
			};

			var handleReferenceContent = function(item, article){
				if(!article.error)
					item.ref_content = article.html;
				articleCallback(null, item);
			};

			async.waterfall([
				checkExisted,
				handleExistingStatus,
				handleArticleDetails,
				handleReferenceContent
			]);
			
		});
	});
	
	async.series(calls,		
	function(err, result) {
		var err = err || null;		
	    callback(err, result);
	});
};

Engine.prototype.translateAndDownloadBanner = function(contentToTranslate, mainCallback){	
	var self = this;
	var downloads = [];

	var translator = new Translator();		
	translator.setSrcContent(contentToTranslate);
	var translate = function(callback){	
		// for testing
		callback(null, null, contentToTranslate);
		
		//actual
		// translator.translateArray(function(err, translatedContent){
		// 	callback(null, err, translatedContent);
		// });		
	}
	var handleTranslatedContent = function(err, translatedContent, callback){
		self.articles = contentToTranslate;		
		for( var i = 0; i < translatedContent.length; i ++){

			// the translatedContent array always has an undefined element at the end of it
			// the reason is still unknown
			if(!translatedContent[i]) continue;

			for( var prop in translatedContent[i]){
				self.articles[i][prop] = translatedContent[i][prop];	
			}

			var url = self.articles[i].banner_url || null;			
			var name = slugifier.slugify(self.articles[i].institution).toLowerCase();
			var ext = url ? url.split("/").pop().split('.').pop() : null;
			self.articles[i].banner_name =  name + "." + ext;				
			self.articles[i].slug = name;

			// skip downloading if banner url is null
			if(!url) continue;

			// download and crop images		
			var createDownloadAndCropFunc = function(url, name, ext){
				return function(dlCallback){
					if(url)
						url = url.replace('https', 'http');
					console.log("\nDownloading: " + url);
					var dl = downloader();
					dl.download(url, "./images/original/" + name + "." + ext, function(filename){
						console.log("Downloaded: " + filename);
						
						var nameArr = filename.split('/').pop().split('.');
						var extension = nameArr.pop();
						var fName = nameArr.pop();
						
						console.log('Cropping: ' + fName + '.' + extension);
						async.parallel([
							function(cropCallback){
								cropper(filename, './images/1366x350/' + fName + "." + extension, 1366, 350, function(){
									console.log('Cropped into 1366x350');
									cropCallback(null);
								});
							},
							function(cropCallback){
								cropper(filename, './images/360x240/' + fName + "." + extension, 360, 240, function(){
									console.log('Cropped into 360x240');
									cropCallback(null);
								});
							},

						], function(){
							dlCallback(null);
						});
					});
				};
			}
			downloads.push(createDownloadAndCropFunc(url, name, ext));	
		}

		async.series(downloads, function(){
			console.log("\nFinished downloading");
			callback(null);
		});
	};

	async.waterfall([
		translate,
		handleTranslatedContent,		
	], function(err, result){
		mainCallback(err, result);
	});
};

Engine.prototype.checkFinalAndRequestNextPage = function(callback){	
	this.results.push(this.articles);
	this.emit('save', this.articles);
	if(this.breakByArticleExisted || !this.nextUrl || this.currentPage > 5){
		callback(null, this.results); 	// final callback
		return true;
	}
	
	var self = this;
	request(this.nextUrl, function(error, response, html){
		console.log('----------------- page num: ' + self.currentPage + '------------------');
		self.parseGroupsOfArticles(html, callback);
	});
};

module.exports = Engine;