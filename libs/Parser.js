var util = require('util');
var EventEmitter = require('events').EventEmitter;

function Parser(strategy){
	this.strategy = strategy;	
	this.saveFinished = false;
	this.mainProcessFinished = false;
}

util.inherits(Parser, EventEmitter);

Parser.prototype.checkAllFinishedAndExit = function(){
	if(this.saveFinished && this.mainProcessFinished)
		process.exit();
};

Parser.prototype.parseItems = function(strHtml, callback){
	// delegate the parsing job to other strategy instances
	this.strategy.parseGroupsOfArticles(strHtml, callback);
	var that = this;
	this.strategy.on('save', function(data){
		that.emit('save', data);			
	})
}

module.exports = Parser;



