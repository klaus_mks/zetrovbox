var mongoose = require('mongoose');
var async = require('async');
var config = require('./../config/config');

mongoose.Promise = global.Promise;
//mongoose.set('debug', true);

module.exports = function(dbName){
	var connection = mongoose.createConnection(config.db.mongoAtlasConnectionString);
	var Source = require(__dirname + "/../models/SourceSchema").getModel(connection);
	var Post = require(__dirname + "/../models/PostSchema").getModel(connection);

	var engine = {};
	engine.getSourceUrl = function(callback){
		Source.find({
			is_published: true
		}).exec(callback);
	};
	
	engine.checkExisted = function(url, callback){
		Post.find({
			url: url
		}, function(err, data){
			if(err)
				console.log(err);
			var existed = data.length > 0;			
			callback(err, existed);
			//connection.close();
		});
	};


	// save data to db
	engine.save = function(data, callback){		
		if(!data)
			return false;
		var items = [];
		data.forEach(function(value, index, array){
			if(typeof value == 'undefined')
				return false;

			items.push(function(cb){
				var deadlineElements = value.deadline ? value.deadline.split('/') : null;
				var deadlineObject  = deadlineElements != null && deadlineElements.length > 2 
					? new Date(deadlineElements[2], deadlineElements[1] - 1, deadlineElements[0]) 
					: null;		// check deadlineElements' length > 2 to make sure parsed string was a date
					
				var post = new Post({
					name: value.institution,
					url: value.url,
					country: value.country,
					area: value.area,
					field: value.field,
					level: value.level,
					deadline: deadlineObject,
					image_url: value.banner_url,
					image_name: value.banner_name,
					content_url: value.ref_url,
					content: value.ref_content,
					slug: value.slug,
					is_published: false
				});

				post.save(function(err){
					if(err)
						console.error(err);
					else
						console.log(post._id + ' save successfully');
					cb();
				});
			});

			// var deadlineElements = value.deadline ? value.deadline.split('/') : null;
			// var deadlineObject  = deadlineElements != null && deadlineElements.length > 2 
			// 	? new Date(deadlineElements[2], deadlineElements[1] - 1, deadlineElements[0]) 
			// 	: null;		// check deadlineElements' length > 2 to make sure parsed string was a date
				
			// var post = new Post({
			// 	name: value.institution,
			// 	url: value.url,
			// 	country: value.country,
			// 	area: value.area,
			// 	field: value.field,
			// 	level: value.level,
			// 	deadline: deadlineObject,
			// 	image_url: value.banner_url,
			// 	image_name: value.banner_name,
			// 	content_url: value.ref_url,
			// 	content: value.ref_content
			// });

			// post.save(function(err){
			// 	if(err)
			// 		console.error(err);
			// 	else
			// 		console.log(post._id + ' save successfully');
			// });
		});

		async.parallel(items, function(err, result){
			callback();
		})
		
	};
	return engine;
};