var request = require('request');

module.exports = function(dbEngine, parserEngine){
	var scraper = {};
	scraper.currentSource = null;
	scraper.start = function(){
		// get a list of urls using dbEngine	
		dbEngine.getSourceUrl(function(err, source){
			if(err || source.length <= 0)
				return false;
			scraper.request(source);			
		});
	};

	// send http request, get response html
	scraper.request = function(source){
		for( var i = 0; i < source.length; i ++ ){
			scraper.currentSource = source[i];
			request(source[i].url, function(error, response, html){
				if(!error)
				{
					var res = {};
					res.domain = scraper.currentSource.domain;
					res.html = html;
					
					// pass html strings to the parserEnginer		
					parserEngine.parseHtml(res);
				}
			});
		}
	};

	return scraper;
	
};