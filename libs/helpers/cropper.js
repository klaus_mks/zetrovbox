var gm = require('gm').subClass({imageMagick: true});
var smartcrop = require('smartcrop-gm');
var fs = require('fs');

function crop(src, dest, width, height, callback) {
    fs.readFile(src, function (err, body ) {
        if(err){
            console.log("Cannot read: " + src);
            callback(error, true);
        }

        smartcrop.crop(body, {width: width, height: height})    
            .then(function(result) {
                var crop = result.topCrop;
                gm(body)
                .crop(crop.width, crop.height, crop.x, crop.y)
                .resize(width, height)
                .write(dest, function(error){
                    callback(error, true);
                });
            })
            .catch(function(error){
                console.log("Cropping error: " + error);
                callback(error, true);
            });        
    });
}

module.exports = crop;

// usage:
// var src = 'uni.png';
// crop(src, 't/thumb.png', 1366, 350);
