var Parser = require('./Parser');
module.exports = function(dbEngine){
	var parser = {};
	parser.parseHtml = function(source){		
		// get html string from scraperEngine		
		// construct a Parser object and its strategy, determine which strategy to use		
		// base on URL source
		var Engine = require('./parsers/' + source.domain + '.parser');				
		var pa = new Parser(new Engine());			

		// use the Parser object to parse html string using initiated strategies (different strategies)				
		// the html contains a list of posts
		pa.parseItems(source.html, function(err, data){
			console.log('Main process is DONE');
			pa.mainProcessFinished = true;	
			pa.checkAllFinishedAndExit();		
		});
			
		// save parsed data 
		pa.on('save', function(data){	
			pa.saveFinished = false;				
			dbEngine.save(data, function(){
				console.log('Data saving process is DONE');
				pa.saveFinished = true;
				pa.checkAllFinishedAndExit();
			});
		});
	};

	return parser;
};