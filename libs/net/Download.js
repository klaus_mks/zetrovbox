var fs = require('fs');
var request = require('request');

module.exports = function(){
	var downloader = {};
	downloader.download = function(url, filename, callback){
		if(!url)
			return false;
		
		ws = fs.createWriteStream(filename);
		ws.on('error', function(err){
			console.log(err);
		});

		request.head(url, function(err, res, body){
			if(!err && res.statusCode === 200 )
			{
				console.log('content-type:', res.headers['content-type']);
				console.log('content-length:', res.headers['content-length']);	
			}
		   
		});
		
		ws.on('finish', function(){
			ws.close(callback(filename));
		})
		request(url).pipe(ws);
		
		
		//request(url).pipe(ws).on('close', callback);
		//request(url).pipe(fs.createWriteStream(filename)).on('close', callback);
	};
	return downloader;
};