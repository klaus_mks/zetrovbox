var config = require('../../config/config');
var request = require('request');
var querystring = require('querystring');
var util = require('util');

var http = require('http');

function Translator(){
	this.token = "";
	this.tokenServiceUrl = config.translation.tokenServiceUrl;
	//this.translateUrl = config.translation.translateUrlSingle;
	this.translateUrl = config.translation.translateUrl;
	this.key = config.translation.key1;		
	
	this.srcContent = [];
	this.destContent = [];
	this.destLang = "en";
	this.srcLang = "vi";

}

Translator.prototype.translateArray = function(callback){
	if(!this.token){
		console.log('requesting token');
		var self = this;
		this.getToken(function(err, token){			
			self.requestTranslation(token, callback);	
	    });	
	}
	else{
		console.log('token already existed');
		this.requestTranslation(this.token, callback);			
	}
}

Translator.prototype.getToken = function(callback){
	console.log('--- start getting token ---');
	if(this.token)
		return false;
	var options = {
	 	url: this.tokenServiceUrl,
	 	method: "POST",
	  	headers: {
	    	'Ocp-Apim-Subscription-Key': this.key
	  	}
	};

	request(options, function(err, response, body){		
		if(!err && response.statusCode == 200){
			this.token = body;
			callback(null, this.token);
		}
		else{
			console.log('status code: ' + response.statusCode);
			console.log('error: ' + err);
		}
	});
	
}

Translator.prototype.requestTranslation = function(token, callback){
	var textArray = "[";
	this.srcContent.forEach(function(ele){
		var i = 0;
		for( var prop in ele){
			if(i >= 5)
				return;
			textArray += '\"' + ele[prop] + '\"' + ", ";
			i ++;
		}			
	});

	textArray += "]";

	var options = {
	   	hostname: config.translation.ajax.hostname,
	   	port: 80,
	   	path: config.translation.ajax.path + '?texts=' 
	     	+ encodeURIComponent(textArray) + '&from='
	     	+ encodeURIComponent(this.srcLang) + '&to='
	     	+ encodeURIComponent(this.destLang),
	   	method: 'GET'
	};

	var req = http.request(options, function (res) {

       	res.setEncoding('utf8');

       	var data = '';
       	res.on('data', function (d) {
         	data += d;
       	});

       	res.on('error', function (err) {
         	console.log(err);
       	});

       	res.on('end', function () {
         	var results = [];
         	var item = {};
         	var labels = ['Institution', 'Country', 'Area', 'Field', 'Level'];

         	var step = 5;

         	data = JSON.parse(data.substring(1));
         	for(var i = 0; i < data.length; i ++){
         		item[labels[i % step].toLowerCase()] = data[i].TranslatedText;
         		if( (i + 1) % step == 0){
         			results.push(item);
         			item = {};
         		}
         	}
         	callback(null, results);
       	});
    });

    req.setHeader('Authorization', 'Bearer ' + token);
    req.end();
}

Translator.prototype.setSrcContent = function(arr){
	this.srcContent = arr;
}

// http://docs.microsofttranslator.com/text-translate.html#!/default/post_TranslateArray
Translator.prototype.tran = function(callback){
	this.getToken(function(err, token){
		requestTranslate(token);			
    });
    
	var self = this;
	var requestTranslate = function(token){
		var textArray = '[' + self.srcContent.map(function (text) { return '\"' + text + '\"' }).join(', ') + ']';  
		var options = {
		   	hostname: config.translation.ajax.hostname,
		   	port: 80,
		   	path: config.translation.ajax.path + '?texts=' 
		     	+ encodeURIComponent(textArray) + '&from='
		     	+ encodeURIComponent(self.srcLang) + '&to='
		     	+ encodeURIComponent(self.destLang),
		   	method: 'GET'
		};

		var req = http.request(options, function (res) {

	       	res.setEncoding('utf8');

	       	var data = '';
	       	res.on('data', function (d) {
	         	data += d;
	       	});

	       	res.on('error', function (err) {
	         	console.log(err);
	       	});

	       	res.on('end', function () {
	         	var r = JSON.parse(data.substring(1));
	         	console.log(r);
	       	});
	    });

	    req.setHeader('Authorization', 'Bearer ' + token);
	    req.end();
	}

	
}

Translator.prototype.translate = function(callback){
	var self = this;
	var translateWork = function(token){
		var options = {
			encoding: 'utf8',
			url: this.translateUrl,
			method: "POST",
			headers: {
				"Authorization" : "Bearer " + token,
			},
			
			body: querystring.stringify({
				texts: this.srcContent,
				to: this.destLang,
				from: this.srcLang
			}),

			// qs: {
			// 	//texts: querystring.stringify(this.srcContent),
			// 	//texts: encodeURIComponent(this.srcContent),
			// 	//appid: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzY29wZSI6Imh0dHBzOi8vYXBpLm1pY3Jvc29mdHRyYW5zbGF0b3IuY29tLyIsInN1YnNjcmlwdGlvbi1pZCI6IjFkMzE3NTAwMDNkYjQ4OWE4ZGNhMDdhZmIyOTEyNDZjIiwicHJvZHVjdC1pZCI6IlRleHRUcmFuc2xhdG9yLkYwIiwiY29nbml0aXZlLXNlcnZpY2VzLWVuZHBvaW50IjoiaHR0cHM6Ly9hcGkuY29nbml0aXZlLm1pY3Jvc29mdC5jb20vaW50ZXJuYWwvdjEuMC8iLCJhenVyZS1yZXNvdXJjZS1pZCI6Ii9zdWJzY3JpcHRpb25zLzM3MzdjNTRlLTI4ZGYtNDc3MC04NjE2LTUxYjJkMWVmZGIwZC9yZXNvdXJjZUdyb3Vwcy90cmFuc2xhdG9yL3Byb3ZpZGVycy9NaWNyb3NvZnQuQ29nbml0aXZlU2VydmljZXMvYWNjb3VudHMva2xhdXMiLCJpc3MiOiJ1cm46bXMuY29nbml0aXZlc2VydmljZXMiLCJhdWQiOiJ1cm46bXMubWljcm9zb2Z0dHJhbnNsYXRvciIsImV4cCI6MTQ4MjMzNzMzOH0.pGCzqvADX55EFQeYrFkRq4bU8iJ-sf_j9JOut89ozEM",
			// 	text: this.srcContent,
			// 	to: this.destLang,
			// 	from: this.srcLang
			// },
			//useQuerystring: true,
			

		};
		console.log(options);

		// request('https://api.microsofttranslator.com/v2/http.svc/Translate?appid=Bearer%20eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzY29wZSI6Imh0dHBzOi8vYXBpLm1pY3Jvc29mdHRyYW5zbGF0b3IuY29tLyIsInN1YnNjcmlwdGlvbi1pZCI6IjFkMzE3NTAwMDNkYjQ4OWE4ZGNhMDdhZmIyOTEyNDZjIiwicHJvZHVjdC1pZCI6IlRleHRUcmFuc2xhdG9yLkYwIiwiY29nbml0aXZlLXNlcnZpY2VzLWVuZHBvaW50IjoiaHR0cHM6Ly9hcGkuY29nbml0aXZlLm1pY3Jvc29mdC5jb20vaW50ZXJuYWwvdjEuMC8iLCJhenVyZS1yZXNvdXJjZS1pZCI6Ii9zdWJzY3JpcHRpb25zLzM3MzdjNTRlLTI4ZGYtNDc3MC04NjE2LTUxYjJkMWVmZGIwZC9yZXNvdXJjZUdyb3Vwcy90cmFuc2xhdG9yL3Byb3ZpZGVycy9NaWNyb3NvZnQuQ29nbml0aXZlU2VydmljZXMvYWNjb3VudHMva2xhdXMiLCJpc3MiOiJ1cm46bXMuY29nbml0aXZlc2VydmljZXMiLCJhdWQiOiJ1cm46bXMubWljcm9zb2Z0dHJhbnNsYXRvciIsImV4cCI6MTQ4MjMzODI2OH0._LcaMOdPP0R-JI7EErSOrmkRg6NiWQGXeT3l7ksBUYU&text=H%E1%BB%8Dc%20b%E1%BB%95ng%20c%E1%BB%A7a%20%C4%90%E1%BA%A1i%20h%E1%BB%8Dc%20Sheffield%20Hallam%2C%20Anh%2C%202017-2018&from=vi&to=en',
		// 	function(err, resp, body){
		// 		console.log(body);
		// 	})

		request(options, function(err, resp, body){
			//console.log(body);
			console.log(resp.statusCode);
			if(!err && resp.statusCode == 200){
				this.destContent = body;				
				console.log(body);
				callback();
			}
			else{
				console.log(err);
				util.inspect(err);
			}
		});
		
	};

	this.getToken(function(err, token){
		translateWork.apply(self, [token]);
	})
}

module.exports = Translator;
