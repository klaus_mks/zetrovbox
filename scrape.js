var dbEngine = require('./libs/dbEngine');
var parserEngine = require('./libs/parserEngine');
var scraperEngine = require('./libs/scraperEngine');

var db = dbEngine('zetrovbox');
var parsingService = parserEngine(db);
var scrapingService = scraperEngine(db, parsingService);

scrapingService.start();