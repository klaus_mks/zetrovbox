var mongoose = require('mongoose');
var schema = mongoose.Schema;
var SourceSchema = new schema({
	name: String,
	url: String,
	domain: String,
	is_published: Boolean,
	created_at: Date,
	modified_at: Date
});

// add current time to every save
// the opposite hook is post
SourceSchema.pre('save', function(next){
	this.modified_at = new Date();
	if(!this.created_at)
		this.created_at = new Date();
	next();
})

module.exports = {
	getModel: function getModel(connection){
		return connection.model("Source", SourceSchema);
	}
};