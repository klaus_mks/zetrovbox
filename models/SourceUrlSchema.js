var mongoose = require('mongoose');
var schema = mongoose.Schema;
var SourceUrlSchema = new schema({
	source_id: String,
	name: String,
	url: String,
	url_hash: String,
	is_processed: Boolean,	
	created_at: Date,
	modified_at: Date
});

module.exports = {
	getModel: function getModel(connection){
		return connection.model("SourceUrl", SourceUrlSchema);
	}
};