var mongoose = require('mongoose');
var schema = mongoose.Schema;
var PostSchema = new schema({
	source_id: String,
	name: String,
	url: String,
	country: String,
	area: String,
	field: String,
	level: String,
	deadline: Date,
	
	image_url: String,
	image_name: String,
	
	content_url: String,
	content: String,
	slug: String,
	is_published: Boolean,

	created_at: Date,
	modified_at: Date
});

PostSchema.virtual('id').get(function(){
	return this._id;
})

PostSchema.pre('save', function(next){
	var now = new Date();
	this.modified_at = now;
	if(!this.created_at)
		this.created_at = now;
	next();
})

module.exports = {
	getModel: function getModel(connection){
		return connection.model("Post", PostSchema);
	}
};